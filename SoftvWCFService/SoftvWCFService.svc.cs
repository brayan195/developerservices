﻿
using Microsoft.IdentityModel.Tokens;
using SoftvWCFService.Contracts;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using Softv.Entities;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net.Mime;
using System.Data;

using SoftvWCFService.Entities;

namespace SoftvWCFService
{
    [ScriptService]
    public partial class SoftvWCFService:ISessionWeb      
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        #region CLIENTE 

        public int? AddDeveloper(string Nombre, int? Op)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("AddDeveloper", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Nombre", Nombre);
                       

                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            //while (rd.Read())
                            //{
                            //    result = Int32.Parse(rd[0].ToString());
                            //}
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }




        public List<DeveloperEntity> GetDeveloper()
        {
            List<DeveloperEntity> lista = new List<DeveloperEntity>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("GetDeveloper", connection);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                DeveloperEntity c = new DeveloperEntity();
                                c.Id = Int32.Parse(rd[0].ToString());
                                c.Nombre = rd[1].ToString();
                                lista.Add(c);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return lista;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }


        public List<DeveloperEntity> GetDeveloperById(int? Id)
        {
            List<DeveloperEntity> lista = new List<DeveloperEntity>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {

                        SqlCommand command = new SqlCommand("GetDevelopersById", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Id", Id);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                DeveloperEntity c = new DeveloperEntity();
                                c.Id = Int32.Parse(rd[0].ToString());
                                c.Nombre = rd[1].ToString();
                                lista.Add(c);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return lista;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }



        public int? UpdateDeveloper(string Nombre, int? Id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("UpdateDeveloper", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Nombre", Nombre);
                        command.Parameters.AddWithValue("@Id", Id);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            //while (rd.Read())
                            //{
                            //    result = Int32.Parse(rd[0].ToString());
                            //}
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }




        public int? AddEmpresa(string Nombre, int? Op)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("AddEmpresa", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Nombre", Nombre);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            //while (rd.Read())
                            //{
                            //    result = Int32.Parse(rd[0].ToString());
                            //}
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }




        public List<DeveloperEntity> GetEmpresa()
        {
            List<DeveloperEntity> lista = new List<DeveloperEntity>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("GetEmpresa", connection);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                DeveloperEntity c = new DeveloperEntity();
                                c.Id = Int32.Parse(rd[0].ToString());
                                c.Nombre = rd[1].ToString();
                                lista.Add(c);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return lista;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }


        public List<DeveloperEntity> GetEmpresaById(int? Id)
        {
            List<DeveloperEntity> lista = new List<DeveloperEntity>();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {

                        SqlCommand command = new SqlCommand("GetEmpresaById", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Id", Id);
                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            while (rd.Read())
                            {
                                DeveloperEntity c = new DeveloperEntity();
                                c.Id = Int32.Parse(rd[0].ToString());
                                c.Nombre = rd[1].ToString();
                                lista.Add(c);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return lista;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }



        public int? UpdateEmpresa(string Nombre, int? Id)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                try
                {
                    int result = 0;
                    string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    using (SqlConnection connection = new SqlConnection(ConnectionString))

                    {
                        SqlCommand command = new SqlCommand("UpdateEmpresa", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Nombre", Nombre);
                        command.Parameters.AddWithValue("@Id", Id);


                        IDataReader rd = null;
                        try
                        {
                            if (connection.State == ConnectionState.Closed)
                                connection.Open();
                            rd = command.ExecuteReader();
                            //while (rd.Read())
                            //{
                            //    result = Int32.Parse(rd[0].ToString());
                            //}
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                        return result;
                    }
                }

                catch (Exception ex)
                {
                    throw new WebFaultException<string>(ex.Message, HttpStatusCode.ExpectationFailed);
                }

            }

        }






















        #endregion
















    }
}
