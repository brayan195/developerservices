﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class CotizacionEntity
    {
        [DataMember]
        public int ? id { get; set; }

        [DataMember]
        public int? id_cotizacion { get; set; }


        [DataMember] 
        public DateTime Fecha { get; set; }

        [DataMember]
        public string FechaCotizacion2 { get; set; }

        [DataMember]
        public DateTime FechaExpiracion { get; set; }

        [DataMember]
        public string FechaExpiracion2 { get; set; }

        [DataMember]
        public string Fecha2 { get; set; }

        [DataMember]
        public int? Clv_cliente { get; set; }

        [DataMember]
        public int? Clv_vendedor { get; set; }

        [DataMember]
        public int? clv_proyecto { get; set; }

        [DataMember]
        public int? Cetes { get; set; }

        [DataMember]
        public int? Multiplicador { get; set; }

        [DataMember]
        public int? Reservacion { get; set; }

        [DataMember]
        public int? Escrituracion { get; set; }

        [DataMember]
        public int? IdDetalle { get; set; }

        [DataMember]
        public int? id_plan_pago { get; set; }

        //[DataMember]
        //public ProyectoEntity Proyecto { get; set; }

        //[DataMember]
        //public EstatusDepartamentoEntity EDepartamento { get; set; }

        //[DataMember]
        //public List<DepartamentoEntity> EDepartamentoC { get; set; }

        [DataMember]
        public int? PeriodoMeses { get; set; }
        
        [DataMember]
        public int? clv_departamento { get; set; }
        [DataMember]
        public int? Clv_prospecto { get; set; }

        [DataMember]
        public string Vendedor { get; set; }

        [DataMember]
        public string Prospecto { get; set; }

        [DataMember]
        public string Estatus { get; set; }

        [DataMember]
        public int? Opexportacion { get; set; }

        [DataMember]
        public int? op { get; set; }
        [DataMember]
        public List<InfoMobiliti> descripcion { get; set; }

    }



    public class ProspectoEntity
    {
        [DataMember]
        public int? Clv_prospecto { get; set; }
   

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Correo { get; set; }

        [DataMember]
        public string Telefono { get; set; }

      


    }
}