﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class DeveloperEntity
    {
        [DataMember]
        public int ? Id { get; set; }

        [DataMember]
        public string Nombre { get; set; } 


    }
}