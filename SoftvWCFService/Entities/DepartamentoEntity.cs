﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class DepartamentoEntity
    {
        [DataMember]
        public int? Clv_departamento { get; set; }

        [DataMember]
        public string TipoDepartamento { get; set; }

        [DataMember]
        public int ? NumeroNiveles { get; set; }

        [DataMember]
        public float? MetrosTotales { get; set; }

        [DataMember]
        public decimal ? CostoDepartamento { get; set; }

        [DataMember]
        public int ? cantidad { get; set; }

        [DataMember]
        public bool? Activo { get; set; }

        [DataMember]
        public List<DepartamentoEspacioEntity> espacios { get; set; }

        [DataMember]
        public string Lado { get; set; }

    }


    [DataContract]
    [Serializable]
    public class DepartamentoEspacioEntity
    {
        [DataMember]
        public int? clv_espacio_departamento { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public float? metros { get; set; }

        [DataMember]
        public bool suma { get; set; }

        [DataMember]
        public string descripcion { get; set; }       
    }
}