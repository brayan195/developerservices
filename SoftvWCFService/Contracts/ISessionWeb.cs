﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Softv.Entities;
using SoftvWCFService.Entities;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface ISessionWeb
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddDeveloper", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddDeveloper(string Nombre, int? Op);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeveloper", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DeveloperEntity> GetDeveloper();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeveloperById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DeveloperEntity> GetDeveloperById(int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateDeveloper", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateDeveloper(string Nombre, int? Id);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "AddEmpresa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? AddEmpresa(string Nombre, int? Op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEmpresa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DeveloperEntity> GetEmpresa();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEmpresaById", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DeveloperEntity> GetEmpresaById(int? Id);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateEmpresa", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? UpdateEmpresa(string Nombre, int? Id);






    }
}

